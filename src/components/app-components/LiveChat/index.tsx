import React, { useState, useRef } from "react";
import styles from "./LiveChat.module.scss";
import { HiUsers } from "react-icons/hi";
import classNames from "classnames/bind";
import MessageItem from "./MessageItem";
import { IoIosPaperPlane } from "react-icons/io";
import { HiDotsHorizontal } from "react-icons/hi";
import { BsArrowBarLeft, BsArrowBarRight, BsChatLeftDotsFill } from "react-icons/bs";
import { v4 as uuidv4 } from "uuid";
const cx = classNames.bind(styles);

const listMessage = [
  {
    userId: "1000",
    userAvatar:
      "https://static.vecteezy.com/system/resources/previews/006/487/912/original/hacker-avatar-ilustration-free-vector.jpg",
    userName: "1st De Phrish",
    userMessage: "A reader will be distracted by the readable content of a page.",
    isOnline: true,
    isCheck: true,
  },
  {
    userId: "1001",
    userAvatar:
      "https://st3.depositphotos.com/1007566/12989/v/950/depositphotos_129895474-stock-illustration-hacker-character-avatar-icon.jpg",
    userName: "2nd De Phrish",
    userMessage: "A reader will be distracted by the readable content of a page.",
    isOnline: true,
    isCheck: false,
  },
  {
    userId: "1002",
    userAvatar: "https://toppng.com/uploads/preview/hacker-avatar-11556286058ge7psro7wu.png",
    userName: "3rd De Phrish",
    userMessage: "A reader will be distracted by the readable content of a page.",
    isOnline: false,
    isCheck: true,
  },
  {
    userId: "1003",
    userAvatar: "https://toppng.com/uploads/preview/hacker-avatar-11556286068ziooyvonc2.png",
    userName: "4th De Phrish",
    userMessage: "A reader will be distracted by the readable content of a page.",
    isOnline: false,
    isCheck: false,
  },
  {
    userId: "1004",
    userAvatar: "https://toppng.com/uploads/preview/hacker-avatar-11556286068ziooyvonc2.png",
    userName: "5th De Phrish",
    userMessage: "A reader will be distracted by the readable content of a page.",
    isOnline: false,
    isCheck: false,
  },
  {
    userId: "1005",
    userAvatar: "https://toppng.com/uploads/preview/hacker-avatar-11556286068ziooyvonc2.png",
    userName: "6th De Phrish",
    userMessage: "A reader will be distracted by the readable content of a page.",
    isOnline: false,
    isCheck: false,
  },
  {
    userId: "1006",
    userAvatar: "https://toppng.com/uploads/preview/hacker-avatar-11556286068ziooyvonc2.png",
    userName: "7th De Phrish",
    userMessage: "A reader will be distracted by the readable content of a page.",
    isOnline: false,
    isCheck: false,
  },
  {
    userId: "1007",
    userAvatar: "https://toppng.com/uploads/preview/hacker-avatar-11556286068ziooyvonc2.png",
    userName: "8th De Phrish",
    userMessage: "A reader will be distracted by the readable content of a page.",
    isOnline: false,
    isCheck: false,
  },
  {
    userId: "1008",
    userAvatar: "https://toppng.com/uploads/preview/hacker-avatar-11556286068ziooyvonc2.png",
    userName: "9th De Phrish",
    userMessage: "A reader will be distracted by the readable content of a page.",
    isOnline: false,
    isCheck: false,
  },
  {
    userId: "1009",
    userAvatar: "https://toppng.com/uploads/preview/hacker-avatar-11556286068ziooyvonc2.png",
    userName: "10th De Phrish",
    userMessage: "A reader will be distracted by the readable content of a page.",
    isOnline: false,
    isCheck: false,
  },
  {
    userId: "10010",
    userAvatar: "https://toppng.com/uploads/preview/hacker-avatar-11556286068ziooyvonc2.png",
    userName: "11th De Phrish",
    userMessage: "A reader will be distracted by the readable content of a page.",
    isOnline: false,
    isCheck: false,
  },
  {
    userId: "10011",
    userAvatar: "https://toppng.com/uploads/preview/hacker-avatar-11556286068ziooyvonc2.png",
    userName: "12th De Phrish",
    userMessage: "A reader will be distracted by the readable content of a page.",
    isOnline: false,
    isCheck: false,
  },
];

const LiveChat = () => {
  const chatRef = useRef<any>();
  const [isCollapsed, setIsCollapsed] = useState<boolean>(false);
  const handleCollapse = () => {
    chatRef.current.classList.toggle("collapsed");
    setIsCollapsed((prevState: boolean) => !prevState);
  };

  return (
    <div className={cx("wrapper", "live-chat")}>
      <iframe
        className={cx("live--wrapper")}
        width="560"
        height="315"
        src="https://www.youtube.com/embed/y_6aSG2yfe8"
        title="YouTube video player"
        frameBorder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
        allowFullScreen
      ></iframe>
      <div className={cx("chat")}>
        <div onClick={handleCollapse} className={cx("button-collapse")}>
          {isCollapsed ? (
            <BsArrowBarLeft className={cx("collapse-icon")} />
          ) : (
            <BsArrowBarRight className={cx("collapse-icon")} />
          )}
        </div>
        <div ref={chatRef} className={cx("chat--wrapper")}>
          <div className={cx("header")}>
            <h3 className={cx("title")}>Live Chat</h3>
            <div className={cx("user")}>
              <HiUsers className={cx("user-icon")} />
              <span className={cx("user-count")}>1,500 people</span>
            </div>
          </div>
          <div className={cx("chat-container")}>
            {listMessage.map((message) => {
              return (
                <MessageItem
                  key={uuidv4()}
                  userId={message.userId}
                  userAvatar={message.userAvatar}
                  userName={message.userName}
                  userMessage={message.userMessage}
                  isOnline={message.isOnline}
                  isCheck={message.isCheck}
                />
              );
            })}
          </div>
          <div className={cx("input-chat--wrapper")}>
            <div className={cx("tool")}>
              <HiDotsHorizontal className={cx("dots")} />
            </div>
            <input className={cx("input-chat")} type="text" placeholder="Write your message" />
            <div className={cx("send-button")}>
              <IoIosPaperPlane className={cx("send-icon")} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default LiveChat;
