import React from "react";
import styles from "./MessageItem.module.scss";
import classNames from "classnames/bind";
import { BsFillCheckCircleFill,BsCheckCircle } from "react-icons/bs";
import { GoPrimitiveDot } from "react-icons/go";
const cx = classNames.bind(styles);

interface IUserChat {
  userId: string;
  userAvatar: string;
  userName: string;
  userMessage: string;
  isOnline: boolean;
  isCheck: boolean;
}

const MessageItem = ({ userId, userAvatar, userName, userMessage, isOnline, isCheck }: IUserChat) => {
  return (
    <div id={userId} className={cx("wrapper")}>
      <div className={cx("image--wrapper")}>
        <img className={cx("image")} src={userAvatar} alt="user-avatar" />
        {isCheck && <BsFillCheckCircleFill className={cx("check-icon")} />}
      </div>
      <div className={cx("text--wrapper")}>
        <div className={cx("user-info")}>
          <div className={cx("user-name")}>{userName}</div>
          {isOnline ? <GoPrimitiveDot className={cx("online")} /> : <GoPrimitiveDot className={cx("offline")} />}
        </div>
        <span className={cx("user-message")}>{userMessage}</span>
      </div>
    </div>
  );
};

export default MessageItem;
